import React, { Component } from 'react';
import { Grid, Row, Col, FormGroup, FormControl, ControlLabel, Radio, Button } from 'react-bootstrap';

export default class FurnitureForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      piece: 2,
      name: "Hello"
    }
	this.handleChangePiece = this.handleChangePiece.bind(this);
  }

  handleChangePiece(ev) {
    // "this" is a component
    // "ev.target" is the control that created the event
    this.setState({piece: ev.target.value});
    console.log('change');
  }

  render() {
    return (
      <div className={(this.state.name.length >= 10) ? "barf" : ""}>
        <select onChange={e => this.handleChangePiece(e)}
          value={this.state.piece}>
          <option value="1">Chair</option>
          <option value="2">Table</option>
          <option value="3">Effigy</option>
          <option value="4">Ottoman</option>
          <option value="5">Wardrobe</option>
        </select>
        {this.state.piece > 3 ? (
          <input type="text" value={this.state.name}
            onChange={e => {
              this.setState({name: e.target.value});
              console.log("text change", this.state);
            }} />
          ) : null}
        </div>
      );
  }
}

FurnitureForm.propTypes = {
  name: React.PropTypes.string,
  parent: React.PropTypes.object,
  mykey: React.PropTypes.number
}
