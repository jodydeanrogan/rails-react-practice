import React, { Component } from 'react';
import FurnitureForm from './furniture_form.jsx';

export default class FurniturePage extends Component {
  constructor(){
    super()
    this.buildState = this.buildState.bind(this)
    this.state = this.buildState()
  }
  buildState() {
    var children = [];
    for (var i = 0; i < 3; ++i) {
      // Build 3 components from FurnitureForm
      children.push(
        <FurnitureForm key={i} mykey={i} name={"Person " + i.toString()} parent={this} />
        // React.createElement(FurnitureForm,
        //     { key: i, mykey: i, name: "Person " + i.toString(), parent: this}
        // )
      );
    }
    return {children}
  }
  render() {

    return (
      <div>
        {this.state.children}
      </div>
    );
  }
}
